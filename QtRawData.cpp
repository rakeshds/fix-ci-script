#include "QtRawData.h"

namespace QtRawData {

    inline byte & ByteRef (void * ptr, const bytecount offset) {
        return static_cast<byte *> (ptr) [offset];
    }

    inline const byte & ConstByteRef (const void * ptr, const bytecount offset) {
        return static_cast<const byte *> (ptr) [offset];
    }

    bytecount::bytecount (const int arg) : _count (arg) { }
    bytecount & bytecount::operator++ (void) { _count++; return (* this); }
    bytecount & bytecount::operator+= (const int arg) { _count += arg; return (* this); }
    bytecount bytecount::operator+ (const int arg) const { return bytecount { _count + arg }; }
    bytecount bytecount::operator* (const int arg) const { return bytecount { _count * arg }; }
    bytecount::operator bitcount (void) const { return toBitCount (); }
    bytecount::operator int (void) const { return _count; }
    bitcount bytecount::toBitCount (void) const { return bitcount { _count * 8 }; }

    bitcount::bitcount (const int arg) : _count (arg) { }
    bitcount & bitcount::operator++ (void) { _count++; return (* this); }
    bitcount & bitcount::operator+= (const int arg) { _count += arg; return (* this); }
    bitcount bitcount::operator+ (const int arg) const { return bitcount { _count + arg }; }
    bitcount bitcount::operator* (const int arg) const { return bitcount { _count * arg }; }
    bitcount::operator int (void) const { return _count; }
    bool bitcount::isAligned (void) const { return bool { _count % 8 == 0 }; }
    bytecount bitcount::toByteCount (void) const { return bytecount { _count / 8 }; }

    namespace UnSafe {

        void copySingleBit (const void * srcBitsPtr, const bitcount srcBitOffset, void * dstBitsPtr, const bitcount dstBitOffset) {
            if ((ConstByteRef (srcBitsPtr, srcBitOffset.toByteCount ()) >> (srcBitOffset % 8)) & 0x1) {
                ByteRef (dstBitsPtr, dstBitOffset.toByteCount ()) |= (0x1 << (dstBitOffset % 8));
            }
            else {
                ByteRef (dstBitsPtr, dstBitOffset.toByteCount ()) &= ~(0x1 << (dstBitOffset % 8));
            }
        }

        void copySingleByte (const void * srcBytesPtr, const bytecount srcByteOffset, void * dstBytesPtr, const bytecount dstByteOffset) {
            ByteRef (dstBytesPtr, dstByteOffset) = ConstByteRef (srcBytesPtr, srcByteOffset);
        }

        void copyAlignedBytes (const void * srcBytesPtr, const bytecount srcBytesOffset, void * dstBytesPtr, const bytecount dstBytesOffset, const bytecount bytesCount) {
            for (int loops {}; loops < bytesCount; ++loops) {
                copySingleByte (srcBytesPtr, (srcBytesOffset + loops), dstBytesPtr, (dstBytesOffset + loops));
            }
        }

        void copyUnalignedBits (const void * srcBitsPtr, const bitcount srcBitsOffset, void * dstBitsPtr, const bitcount dstBitsOffset, const bitcount bitsCount) {
            for (int loops {}; loops < bitsCount; ++loops) {
                copySingleBit (srcBitsPtr, (srcBitsOffset + loops), dstBitsPtr, (dstBitsOffset + loops));
            }
        }

    }

    namespace SafeByPtr {

        void cloneBits (const void * srcDataPtr, const bytecount srcDataSize, const bitcount srcBitsOffset, void * dstDataPtr, const bytecount dstDataSize, const bitcount dstBitsOffset, const bitcount bitsCount) {
            if (srcDataPtr != nullptr && dstDataPtr != nullptr) { // check non null pointers
                if (bitsCount > 0 && srcBitsOffset >= 0 && dstBitsOffset >= 0) { // check validity of boundaries
                    if (srcDataSize.toBitCount () >= (srcBitsOffset + bitsCount)) { // check source size
                        if (dstDataSize.toBitCount () >= (dstBitsOffset + bitsCount)) { // check destination size
                            if (bitsCount == 1) { // single bit
                                UnSafe::copySingleBit (srcDataPtr, srcBitsOffset, dstDataPtr, dstBitsOffset);
                            }
                            else if (srcBitsOffset.isAligned () && dstBitsOffset.isAligned () && bitsCount.isAligned ()) { // aligned bytes
                                UnSafe::copyAlignedBytes (srcDataPtr, srcBitsOffset.toByteCount (), dstDataPtr, dstBitsOffset.toByteCount (), bitsCount.toByteCount ());
                            }
                            else { // unaligned bits
                                UnSafe::copyUnalignedBits (srcDataPtr, srcBitsOffset, dstDataPtr, dstBitsOffset, bitsCount);
                            }
                        }
                    }
                }
            }
        }

    }
}
