#ifndef QTRAWDATA_H
#define QTRAWDATA_H

#include <QDebug>

namespace QtRawData {

    using byte = unsigned char;

    class bitcount;
    class bytecount;

    class bitcount {
        int _count = -1;

    public:
        explicit bitcount (const int arg);

        bitcount & operator++ (void);
        bitcount & operator+= (const int arg);

        bitcount operator+ (const int arg) const;
        bitcount operator* (const int arg) const;

        operator int (void) const;

        inline bool isAligned (void) const;
        inline bytecount toByteCount (void) const;
    };

    class bytecount {
        int _count = -1;

    public:
        explicit bytecount (const int arg);

        bytecount & operator++ (void);
        bytecount & operator+= (const int arg);

        bytecount operator+ (const int arg) const;
        bytecount operator* (const int arg) const;

        operator bitcount (void) const;
        operator int (void) const;

        inline bitcount toBitCount (void) const;
    };

}

inline QtRawData::bitcount as_bitcount (const int count) {
    return QtRawData::bitcount { count };
}

inline QtRawData::bytecount as_bytecount (const int count) {
    return QtRawData::bytecount { count };
}

inline QtRawData::bitcount operator"" _bits (const unsigned long long int count) {
    return as_bitcount (int (count));
}

inline QtRawData::bytecount operator"" _bytes (const unsigned long long int count) {
    return as_bytecount (int (count));
}

inline QDebug operator<< (QDebug debug, const QtRawData::bytecount count) {
    QDebugStateSaver saver (debug);
    debug.nospace () << int (count) << (int (count) > 1 ? "_bytes" : "_byte");
    return debug;
}

inline QDebug operator<< (QDebug debug, const QtRawData::bitcount count) {
    QDebugStateSaver saver (debug);
    debug.nospace () << int (count) << (int (count) > 1 ? "_bits" : "_bit");
    return debug;
}

namespace QtRawData {

    namespace UnSafe { // very fast but no sanity checks

        void copySingleBit     (const void * srcBitsPtr,  const bitcount  srcBitOffset,   void * dstBitsPtr,  const bitcount  dstBitOffset);
        void copySingleByte    (const void * srcBytesPtr, const bytecount srcByteOffset,  void * dstBytesPtr, const bytecount dstByteOffset);
        void copyAlignedBytes  (const void * srcBytesPtr, const bytecount srcBytesOffset, void * dstBytesPtr, const bytecount dstBytesOffset, const bytecount bytesCount);
        void copyUnalignedBits (const void * srcBitsPtr,  const bitcount  srcBitsOffset,  void * dstBitsPtr,  const bitcount  dstBitsOffset,  const bitcount  bitsCount);

    }

    namespace SafeByPtr { // sanity checked version with little overhead

        void cloneBits (const void *    srcDataPtr,
                        const bytecount srcDataSize,
                        const bitcount  srcBitsOffset,
                        void *          dstDataPtr,
                        const bytecount dstDataSize,
                        const bitcount  dstBitsOffset,
                        const bitcount  bitsCount);

    }

    namespace SafeByRef {

        template<typename T_dst, typename T_src> T_dst getBitsAs (const T_src & srcData,
                                                                  const bitcount srcBitsOffset = 0_bits,
                                                                  const bitcount bitsCount     = bytecount (sizeof (T_dst))) {
            T_dst dstData {};
            SafeByPtr::cloneBits (&srcData, as_bytecount (sizeof (T_src)), srcBitsOffset, &dstData, as_bytecount (sizeof (T_dst)), 0_bits, bitsCount);
            return dstData;
        }

        template<typename T_dst, typename T_src> void setBitsAs (const T_src & srcData,
                                                                 T_dst & dstData,
                                                                 const bitcount dstBitsOffset = 0_bits,
                                                                 const bitcount bitsCount     = bytecount (sizeof (T_src))) {
            SafeByPtr::cloneBits (&srcData, as_bytecount (sizeof (T_src)), 0_bits, &dstData, as_bytecount (sizeof (T_dst)), dstBitsOffset, bitsCount);
        }

    }

}

#endif // QTRAWDATA_H
