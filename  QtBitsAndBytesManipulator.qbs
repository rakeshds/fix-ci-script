import qbs;

Project {
    name: "QtBitsAndBytesManipulator";
    minimumQbsVersion: "1.7.0";
    references: [];

    Product {
        name: "app-QtBitsAndBytesManipulator";
        type: "application";
        targetName: "QtBitsAndBytesManipulator";
        cpp.rpaths: ["$ORIGIN", "$ORIGIN/lib"];
        cpp.cxxLanguageVersion: "c++11";

        Depends {
            name: "cpp";
        }
        Depends {
            name: "Qt";
            submodules: ["core"];
        }
        Group {
            name: "C++ files";
            files: [
                "QtRawData.cpp",
                "QtRawData.h",
                "main.cpp",
            ]
        }
        Group {
            qbs.install: true;
            fileTagsFilter: product.type;
        }
    }
}
