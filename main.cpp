#include <QCoreApplication>
#include <QStringBuilder>
#include <QByteArray>

#include "QtRawData.h"

template<typename T> inline QByteArray hex (const T num) {
    return (QByteArray::number (num, 16).toUpper () % "h");
}

int main (int argc, char * argv []) {
    QCoreApplication (argc, argv);
    quint64 src { 0x1234567890ABCDEF };
    quint8  dst { 0 };
    qDebug () << ">>>>>>>>>> src=" << hex (src);
    for (QtRawData::bytecount idx { 0 }; idx < int (sizeof (src) / sizeof (dst)); ++idx) {
        QtRawData::UnSafe::copySingleByte (&src, idx, &dst, 0_bytes);
        qDebug () << ">>>>>>>>>> copySingleByte at offset" << idx << ":" << "dst=" << hex (dst);
    }
    qDebug () << ">>>>>>>>>> getBitsAs bool   at offset 0_byte :" << "ret=" <<      QtRawData::SafeByRef::getBitsAs<bool>    (src, 0_bits, 1_bits);
    qDebug () << ">>>>>>>>>> getBitsAs uint4  at offset 0_byte :" << "ret=" << hex (QtRawData::SafeByRef::getBitsAs<quint8>  (src, 0_bits, 4_bits));
    qDebug () << ">>>>>>>>>> getBitsAs uint8  at offset 0_byte :" << "ret=" << hex (QtRawData::SafeByRef::getBitsAs<quint8>  (src));
    qDebug () << ">>>>>>>>>> getBitsAs uint12 at offset 0_byte :" << "ret=" << hex (QtRawData::SafeByRef::getBitsAs<quint16> (src, 0_bits, 12_bits));
    qDebug () << ">>>>>>>>>> getBitsAs uint16 at offset 0_byte :" << "ret=" << hex (QtRawData::SafeByRef::getBitsAs<quint16> (src));
    qDebug () << ">>>>>>>>>> getBitsAs uint32 at offset 0_byte :" << "ret=" << hex (QtRawData::SafeByRef::getBitsAs<quint32> (src));
    qDebug () << ">>>>>>>>>> getBitsAs uint64 at offset 0_byte :" << "ret=" << hex (QtRawData::SafeByRef::getBitsAs<quint64> (src));
    QtRawData::byte frame [] { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
    qDebug () << ">>>>>>>>>> frame=" << QByteArray::fromRawData (reinterpret_cast<const char *> (frame), sizeof (frame)).toHex ();
    qDebug () << ">>>>>>>>>> getBitsAs int16 at offset 2_bytes :" << "ret=" << hex (QtRawData::SafeByRef::getBitsAs<quint16> (frame, 2_bytes));
    QtRawData::byte buffer [10] { };
    qDebug () << ">>>>>>>>>> buffer=" << QByteArray::fromRawData (reinterpret_cast<const char *> (buffer), sizeof (buffer)).toHex ();
    QtRawData::SafeByRef::setBitsAs (qint16 (0x1234), buffer, 24_bits);
    qDebug () << ">>>>>>>>>> setBitsAs int16  at offset 24_bits :" << "buffer=" << QByteArray::fromRawData (reinterpret_cast<const char *> (buffer), sizeof (buffer)).toHex ();
    QtRawData::byte reference [] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    qDebug () << ">>>>>>>>>> reference=" << QByteArray::fromRawData (reinterpret_cast<const char *> (reference), sizeof (reference)).toHex ();
    QtRawData::byte replicated [sizeof (reference)] { };
    qDebug () << ">>>>>>>>>> replicated=" << QByteArray::fromRawData (reinterpret_cast<const char *> (replicated), sizeof (replicated)).toHex ();
    const QtRawData::bytecount size { sizeof (replicated) };
    QtRawData::SafeByPtr::cloneBits (reference, size, 0_bits, replicated, size, 0_bits, size);
    qDebug () << ">>>>>>>>>> cloneBits :" << "replicated=" << QByteArray::fromRawData (reinterpret_cast<const char *> (replicated), sizeof (replicated)).toHex ();
    return 0;
}
